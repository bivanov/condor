Welcome to Condor!
==================================

Condor is a Grunt-alike automation system. It allows user to perform chains of operations 
with simple scripts, watch files and directories with further reaction to changes and so on

Please, refer to documentation - http://bivanov.bitbucket.com/condor. Please be noticed that
while Condor in constant development phase docs may not cover all or latest functionality. 